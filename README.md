Enable this lab feature...

> ![](lab_feature.png)

clear your browser cache, reload the page, and Santa will appear...

<img src="santa.jpg" width="150" height="150"></img>

Addresses [site/master issue BB-6931](https://bitbucket.org/site/master/issues/6930/support-some-or-all-html-in-markdown-bb)
